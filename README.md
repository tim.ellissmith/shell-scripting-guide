# Shell scripting Guide

## When to use bash
Bash, while highly useful becomes cumbersome when used for overly complex tasks. My general rule has been to avoid using bash for arrays or when the script seems like it will be overly difficult to manage.

Bash is very useful when you will be dealing primarly with command line tools, but if you are considering using specialist libraries or dealing with arrays, then Python may be a better choice.

## The script name
According to Google's style guide, executable files should not have an extension, while library files should do (and should not be exectutable). e.g.
```
#! /usr/bin/env bash
# filename test

source testlib.sh

```

## Script layout
Your script should generally be laid out in the following order:
1. Shebang
2. Script descriptions
3. Included libraries
4. Global variables
5. Functions
6. Main function (optional)
7. Executable code



## The Shebang
Using /usr/bin/env bash is generally more portable the /bin/bash
```
/usr/bin/env bash
```


## Script descriptions
It's generally a good idea to havea file header which explains what the script does:
```
# Title: A sample bash script
# Description: Automatically run widget job
# Author: Joe Bloggs
# Date: 11 September 2016
# Version: 0.01
# Usage: ./script
# Notes: Nothing useful here, please move right along
# Bash version: 4.3.42
```
##  Variables
* Generally variables should be lower-case, with underspaces to separate words
* Constants, however, should be all caps
* Variables should be referenced with {}s
* Variables should be quoted
* Read-only variables should be declared as such
* e.g.

```
readonly DATABASE_DIR='/var/lib/postgresql-9.3'
# or
declare -r DATABASE_DIR='/var/lib/postgresql-9.3'

for script in "${script_file"; do
  "${script}"
done
```

## Command substituion
* $() should be used instead of backticks ``
e.g.
```
$(cat file1) # not `cat file1`
```

## Arrays
### Normal
### Two dimensional
### Hash

## Common functions
Below are some common functions that may be useful when scripting

### Logging function
A useful function can be to log errors and exit if necessary. This can either be separated into two new functions or combined into a single function as follows:
```
###############################################################################
# Log based on status and exit if necessary
# Globals: None
# Arguments:
#   $1 Log Level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
#   $2 Log comment
#   $3 Exit status (none if no exit)
# Returns:
#   None
##############################################################################
function log_and_optional_exit() {
  DATE=$(date +%d-%m-%Y %H:%M)
  if [ "${1}" = 'DEBUG' -o "${1}" = 'INFO' -o "${1}" = 'WARNING' ]; then
    echo "${DATE}" "${1}" "${2}" >> "${LOG_FILE}"
  elif [ "${1}" = 'ERROR' -o "${1}" = 'CRITICAL' ]; then
    echo "${DATE}" "${1}" "${2}" >&2 >> "${LOG_FILE}"
  fi

  if [[ -n $3 ]]; then
    exit $3
  fi
}
```
### GetOpts function
```
###############################################################################
# Function to get the options and parameters for the file
# Globals: None
# Arguments: $@
# Returns:
#  None
###############################################################################
function get_options() {
  while getopts ":ab:" opt; do
  a)
    echo "Option a was used"
  b)
    echo "b was used with argument ${OPTARG}"
  h)
    print_help
  *)
    log_and_optional exit ERROR "Invalid usage -${OPTARG}" 1
}

```
### Colour
```

```


## Filenames

## Main function in long code

## Tests
### Tests to see if a variable is present or absent
### Regex matching

## Code description and comments

## Quoting

## A skeleton bash file

## References

## Links
